# Gdb Avr


## GNU Debugger for avr

This package has been compiled to target the  avr architecture.
GDB is a source-level debugger, capable of breaking programs at
any specific line, displaying variable values, and determining
where errors occurred. Currently, it works for C, C++, Fortran
Modula 2 and Java programs. A must-have for any serious
programmer.

This package is primarily for avr developers and cross-compilers and
is not needed by normal users or developers.
